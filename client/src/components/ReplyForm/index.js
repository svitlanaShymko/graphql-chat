import React, { useState } from 'react';
import { Mutation } from "react-apollo";
import {Card, CardContent, CardActions, TextField, Button, makeStyles} from '@material-ui/core';
import { POST_REPLY_MUTATION } from "../../queries";


const ReplyForm = ({ messageId, toggleForm }) => {
  const [text, setText] = useState('');

  return (
    <Mutation
      mutation={POST_REPLY_MUTATION}
      variables={{ messageId, text }}
      update={() => {
        toggleForm(false);
      }}
    >
      {postMutation => (
        <Card>
          <CardContent>
          <TextField
          fullWidth
          label="Reply"
          multiline
          rows={4}
          defaultValue="Default Value"
          value={text}
          variant="outlined"
          onChange={e => setText(e.target.value)}
          />
          </CardContent>
          <CardActions>
          <Button
             variant="contained" color="primary"
             onClick={()=>postMutation(text)}
             disabled={!text.length}
             >
               Send
          </Button>
          <Button
            onClick={() => toggleForm(false)} variant="contained" color="primary"
          >
            Cancel
          </Button>
          </CardActions>
        </Card>
      )}      
    </Mutation>
  )
}

export default ReplyForm;