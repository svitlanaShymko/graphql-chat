import React, { useState } from 'react';
import { Mutation } from "react-apollo";
import {TextField, Button} from '@material-ui/core';
import { POST_MESSAGE_MUTATION } from '../../queries';

import './style.css';

const AddMessage = props => {
  const [text, setText] = useState('');

  return (
    <div className="add-message container">
      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ text }}
        update={(store, { data: { postMessage } }) => {

        }}
        onCompleted={() => setText('')}
      >
        {messageMutation => (
          <div className="add-message-container">
        <TextField
        label="Write your message"
        multiline
        rows={4}
        defaultValue="Default Value"
        value={text}
        variant="outlined"
        onChange={e => setText(e.target.value)} />

        <Button
          variant="contained" color="primary"
          onClick={()=>messageMutation(text)}
          disabled={!text.length}
        >
          Send
        </Button>
        </div>
        )}
      </Mutation>
    </div>
  )
}

export default AddMessage;