import React from 'react';
import { Mutation } from "react-apollo";
import { ADD_LIKE_MUTATION } from "../../queries";
import FavoriteIcon from '@material-ui/icons/Favorite';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';

const MessageActions = ({ messageId, likes, dislikes, onReply }) => {
  return (
    <Mutation
      mutation={ADD_LIKE_MUTATION}
    >
      {likeMutation => (
        <>
        <IconButton color="primary" aria-label="like" component="span"             onClick={e => {
          likeMutation({ variables: {messageId, value: true } });
        }}>
          <Badge badgeContent={likes} color="secondary"><FavoriteIcon /></Badge>
        </IconButton>
          <IconButton color="primary" aria-label="like" component="span"             onClick={e => {
            likeMutation({ variables: {messageId, value: false } });
          }}>
            <Badge badgeContent={dislikes} color="secondary"><ThumbDownIcon /></Badge>
        </IconButton>
          <Button
            onClick={onReply} variant="contained" color="primary"
          >
            Reply
          </Button>
          </>
      )}

    </Mutation>
  )
}

export default MessageActions;