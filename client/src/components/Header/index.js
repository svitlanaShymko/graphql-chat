import React from 'react';
import { SORT_MESSAGE_OPTIONS } from '../../constants';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import './Header.css';

const Header = ({setOrder, orderBy}) => {

  const handleChange = (event) => {
    setOrder(event.target.value);
  }

  return (
    <div className="header container">
      <div>
        <Select
          value={orderBy}
          onChange={handleChange}
        >
          {SORT_MESSAGE_OPTIONS.map(({key,text, value }) =>
          <MenuItem key={key} value={value}>{text}</MenuItem>)
          }
        </Select>
      </div>
    </div>
  )
}

export default Header;