import React from 'react';
import { Card, CardContent, Typography, makeStyles} from '@material-ui/core';


const useStyles = makeStyles({
  root: {
    textAlign: 'right',
  },
});

const Reply = ({ id, text }) => {
  const classes=useStyles();

  return (
    <Card className={classes.root}>
    <CardContent>
      <Typography color="textSecondary" gutterBottom>
      {id}
      </Typography>
      <Typography variant="main" component="p">
    {text}
      </Typography>
    </CardContent>
    </Card>
  );
};

export default Reply;