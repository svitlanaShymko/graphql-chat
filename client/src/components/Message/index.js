import React, { useState } from 'react';
import RepliesList from '../RepliesList';
import ReplyForm from '../ReplyForm';
import MessageActions from '../MessageActions';
import { makeStyles } from '@material-ui/core/styles';
import {Card, CardActions, CardContent, Typography} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    textAlign: 'left',
  },
});

const Message = ({ id, likes, dislikes, text, replies, createdAt }) => {
  const [isFormShown, toggleForm] = useState(false);
  const classes = useStyles();

  return (
    <>
    <Card className={classes.root}> 
    <CardContent>
      <Typography color="textSecondary" gutterBottom>
      {id}, {createdAt}
      </Typography>
      <Typography variant="main" component="p">
    {text}
      </Typography>
    </CardContent>
    <CardActions>
    <MessageActions
          messageId={id}
          likes={likes}
          dislikes={dislikes}
          onReply={() => toggleForm(!isFormShown)}
        />
    </CardActions>
  </Card>
      <RepliesList replies={replies} />
      {isFormShown &&
        <ReplyForm
          messageId={id}
          toggleForm={toggleForm}
        />
      }
    </>
  )
}

export default Message;