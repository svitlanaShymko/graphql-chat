import React from 'react';
import Reply from '../Reply';

const RepliesList = props => {
  const { replies } = props;
  return (
      replies.map(reply => <Reply key={reply.id} {...reply}/>)
  )
}

export default RepliesList;