import React, { useState } from 'react';
import './App.css';
import MessageList from './components/MessagesList';
import Header from './components/Header';
import AddMessage from './components/AddMessage';
import { SORT_MESSAGE_OPTIONS } from './constants';

function App() {
  const [orderBy, setOrder] = useState(SORT_MESSAGE_OPTIONS[0].value);

  return (    
    <div className="App">
      <header className="App-header">
        <Header setOrder={setOrder} orderBy={orderBy}/>
      </header>
      <div className="container">
        <MessageList orderBy={orderBy}
        />
      </div> 
      <AddMessage />           
    </div>
  );
}

export default App;
