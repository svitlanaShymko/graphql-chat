export const SORT_MESSAGE_OPTIONS = [
    {
      key: 'createdAt_ASC',
      text: 'Oldest',
      value: 'createdAt_ASC',
    },
    {
        key: 'createdAt_DESC',
        text: 'Newest',
        value: 'createdAt_DESC',
    },
    {
      key: 'likes_DESC',
      text: 'Most likes',
      value: 'likes_DESC',
    },
    {
      key: 'dislikes_DESC',
      text: 'Most dislikes',
      value: 'dislikes_DESC',
    }
  ];